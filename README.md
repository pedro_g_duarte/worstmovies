*** SUBIR A APLICAÇÃO ***

Para rodar a aplicação basta clicar com o botão direito em cima da classe "WorstMoviesController"
que fica localizada dentro do pacote "br.com.worstmovies", encontrar a opção "Run As"
e selecionar a opção "Java Application".

Para chamar a API e buscar o retorno JSON conforme especificação, o link será "http://localhost:8080/getPrizeRange".

OBS: Por padrão a porta configurada é a 8080, logo, ela não pode estar sendo utilizada por outra aplicação.

*** ALTERAR CONJUNTO DE DADOS ***

Para alterar o conjunto de dados basta substituir o arquivo "movielist.csv" dentro de "src/main/resources",
O arquivo deve possuir o mesmo nome e extensão, a formatação deverá ser "UTF-8" e o separador deverá ser ";".

OBS: Durante a leitura dos dados foi considerado que os separadores para os nomes dos produtores
são ", " e " and ", pois foi entendido que um produtor pode ganhar um prêmio trabalhando sozinho ou em conjunto.

*** RODAR TESTES UNITÁRIOS ***

Para rodar os testes unitários basta clicar com o botão direito em cima do projeto
encontrar a opção "Run As" e selecionar a opção "JUnit Test".

OBS: Foram criados testes unitários apenas para garantir que o nome dos produtores
estão de acordo com o que pedido na especificação utilizando a fonte de dados inicial.