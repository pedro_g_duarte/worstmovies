package br.com.worstmovies;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import br.com.worstmovies.reader.WorstMoviesReader;
import br.com.worstmovies.service.WorstMoviesService;

public class WorstMoviesTest {

	@Before
	public void populateWorstMoviesList() {
		WorstMoviesReader.readWorstMoviesList();
	}
	
	/*
	 * Testa se o nome do produtor que obteve dois prêmios mais rápido é Joel Silver
	 * */
	@Test
	public void testMinPeriodWinners() {
		try {
			WorstMoviesService worstMoviesService = new WorstMoviesService();
			JSONObject object = worstMoviesService.getPrizeRange();
			JSONArray array = (JSONArray) object.get("min");
			String producerName = (String) array.getJSONObject(0).get("producer");
			assertEquals(producerName, "Joel Silver");
		} catch (JSONException e) {
			fail("Erro ao testar o nome dos produtores que obteram dois prêmios mais rápido");
			e.printStackTrace();
		}
	}
	
	/*
	 * Testa se o nome do produtor com maior intervalo entre dois prêmios é Matthew Vaughn
	 * */
	@Test
	public void testMaxPeriodWinners() throws JSONException {
		try {
			WorstMoviesService worstMoviesService = new WorstMoviesService();
			JSONObject object = worstMoviesService.getPrizeRange();
			JSONArray array = (JSONArray) object.get("max");
			String producerName = (String) array.getJSONObject(0).get("producer");
			assertEquals(producerName, "Matthew Vaughn");
		} catch (JSONException e) {
			fail("Erro ao testar o nome dos produtores com maior intervalo entre dois prêmios");
			e.printStackTrace();
		}
	}

}
