package br.com.worstmovies.controller;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.worstmovies.service.WorstMoviesService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class WorstMoviesController {

	@Autowired
	private WorstMoviesService worstMoviesService;
	
	@GetMapping(path = "/getPrizeRange")
	public String findAllWithoutItems() throws JSONException {
		return worstMoviesService.getPrizeRange().toString();
	}

}
