package br.com.worstmovies.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.worstmovies.pojo.WorstMoviesPojo;

public class WorstMoviesReader implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public static List<WorstMoviesPojo> worstMovies;
	
	public static void readWorstMoviesList() {
		List<WorstMoviesPojo> worstMovies = getWorstMoviesList();
		//Sempre que for iniciado o processo de leitura será limpa a lista.
		//Assim garantindo que a lista sempre estará de acordo com o CSV.
		worstMovies.clear();

		InputStream inputStream = WorstMoviesReader.class.getResourceAsStream("/movielist.csv");
		if (inputStream != null) {
			BufferedReader br = null;
	        String line = "";
	        Integer lineNumber = 1; //inicia em 1 porque a primeira linha é ignorada

	        try {
				br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
				br.readLine(); //ignora a primeira linha

				while ((line = br.readLine()) != null) {
					String[] lineSpl = line.split(";");
					worstMovies.add(new WorstMoviesPojo(Integer.valueOf(lineSpl[0]), lineSpl[1], lineSpl[2], lineSpl[3], (lineSpl.length == 5 && "yes".equalsIgnoreCase(lineSpl[4]))));
					lineNumber++;
				}
			} catch(Exception e) { //foi colocado a Exception genérica, desta forma todo erro que ocorrer na leitura será considerado como um erro na planilha
				System.out.println("Erro na leitura da linha " + lineNumber + ". Todas as colunas são obrigatórias exceto a coluna \"winners\", a coluna year deve ser um número inteiro no formato YYYY");
				worstMovies.clear(); // Caso a planilha possua algum erro será limpa a lista com os indicados
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			System.out.println("Não foi encontrado o arquivo \"movielist.csv\" ");
		}
	}
	
	public static List<WorstMoviesPojo> getWorstMoviesList() {
		if (worstMovies == null) {
			worstMovies = new ArrayList<WorstMoviesPojo>();
		}
		
		return worstMovies;
	}
	
}
