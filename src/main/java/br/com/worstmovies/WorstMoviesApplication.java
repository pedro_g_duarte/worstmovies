package br.com.worstmovies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import br.com.worstmovies.reader.WorstMoviesReader;

@SpringBootApplication
public class WorstMoviesApplication {
	
	public static ConfigurableApplicationContext context;
	
	public static void main(String[] args) {
		context = SpringApplication.run(WorstMoviesApplication.class, args);
		WorstMoviesReader.readWorstMoviesList();
	}
	
}
