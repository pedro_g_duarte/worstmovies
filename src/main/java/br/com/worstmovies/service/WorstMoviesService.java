package br.com.worstmovies.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import br.com.worstmovies.pojo.WorstMoviesPojo;
import br.com.worstmovies.reader.WorstMoviesReader;

@Service
public class WorstMoviesService {
	
	private final String separator = ", ";
	
	private final String separatorAnd = " and ";
	
	public JSONObject getPrizeRange() throws JSONException {
		JSONObject object = new JSONObject();

		if (WorstMoviesReader.getWorstMoviesList().size() == 0) {
			object.put("message", "Não foi encontrado nenhum indicado.");
		} else {
			//coloca em um map os vencedores e o ano em que venceram o prêmio
			Map<String, List<Integer>> winnersMap = this.generateWinnersMap();
			//busca os produtores que levaram menos tempo a ganhar dois prêmios
			object.put("min", this.getMinPeriodWinners(winnersMap));
			//busca os produtores que tiveram um maior intervalo entre dois prêmios
			object.put("max", this.getMaxPeriodWinners(winnersMap));
		}

		return object;
	}
	
	private JSONArray getMinPeriodWinners(Map<String, List<Integer>> winnersMap) throws JSONException {
		JSONArray jsonArray = new JSONArray();
		//lista com o nome dos produtores com menor período entre dois prêmios
		List<String> winnersName = new ArrayList<String>();
		Integer interval = null;
		
		for (Entry<String, List<Integer>> entry : winnersMap.entrySet()) {
			if (entry.getValue().size() > 1) {
				Collections.sort(entry.getValue());
				//busca o primeiro período entre os prêmios
				Integer actualInterval = entry.getValue().get(1) - entry.getValue().get(0);

				if (interval == null) {
					interval = actualInterval;
				}

				//limpa a lista caso seja um período menor
				if (actualInterval < interval) {
					winnersName.clear();
				}

				if (actualInterval <= interval) {
					interval = actualInterval;
					winnersName.add(entry.getKey());
				}
			}
		}
		
		for (String winner : winnersName) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("producer", winner);
			jsonObject.put("interval", interval);
			jsonObject.put("previousWin", winnersMap.get(winner).get(0));
			jsonObject.put("followingWin", winnersMap.get(winner).get(1));
			jsonArray.put(jsonObject);
		}
		
		return jsonArray;
	}
	
	private JSONArray getMaxPeriodWinners(Map<String, List<Integer>> winnersMap) throws JSONException {
		JSONArray jsonArray = new JSONArray();
		//lista com o nome dos produtores que tiveram o maior intervalo entre dois prêmios
		List<String> winnersName = new ArrayList<String>();
		Integer interval = null;
		
		for (Entry<String, List<Integer>> entry : winnersMap.entrySet()) {
			if (entry.getValue().size() > 1) {
				Collections.sort(entry.getValue());
				//busca o maior período entre os prêmios deste produtor
				Integer actualInterval = this.getLongerInterval(entry.getValue());

				if (interval == null) {
					interval = actualInterval;
				}

				//limpa a lista caso seja um período maior
				if (actualInterval > interval) {
					winnersName.clear();
				}

				if (actualInterval >= interval) {
					interval = actualInterval;
					winnersName.add(entry.getKey());
				}
			}
		}
		
		for (String winner : winnersName) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("producer", winner);
			jsonObject.put("interval", interval);
			jsonObject.put("previousWin", winnersMap.get(winner).get(0));
			jsonObject.put("followingWin", winnersMap.get(winner).get(1));
			jsonArray.put(jsonObject);
		}
		
		return jsonArray;
	}
	
	private Integer getLongerInterval(List<Integer> years) {
		Integer interval = null;
		Integer lastYear = null;
		
		for (Integer year : years) {
			if (lastYear != null) {
				Integer actualInterval = year - lastYear;
				interval = (interval == null || interval > actualInterval ? actualInterval : interval);
			}
			
			lastYear = year;
		}
		
		return interval;
	}
	
	private Map<String, List<Integer>> generateWinnersMap() {
		Map<String, List<Integer>> winnersMap = new HashMap<String, List<Integer>>();
		
		for (WorstMoviesPojo pojo : WorstMoviesReader.getWorstMoviesList()) {
			if (pojo.isWinner()) {
				this.addProducers(winnersMap, pojo);
			}
		}
		
		return winnersMap;
	}
	
	private void addProducers(Map<String, List<Integer>> winnersMap, WorstMoviesPojo pojo) {
		//se for um único produtor a vencer coloca ele no map de produtores vencedores
		if (!pojo.getProducers().contains(separator) && !pojo.getProducers().contains(separatorAnd)) {
			this.addMap(winnersMap, pojo.getYear(), pojo.getProducers());
		} else {
			//lógica responsável por adicionar todos os produtores do filme vencedor
			if (pojo.getProducers().contains(separatorAnd)) {
				String[] producers = pojo.getProducers().split(separatorAnd);
				this.splitSeparator(winnersMap, pojo, producers[0]);
				this.addMap(winnersMap, pojo.getYear(), producers[1]);
			} else if (pojo.getProducers().contains(separator)) {
				this.splitSeparator(winnersMap, pojo, pojo.getProducers());
			}
		}
	}
	
	private void splitSeparator(Map<String, List<Integer>> winnersMap, WorstMoviesPojo pojo, String producers) {
		if (!pojo.getProducers().contains(separator)) {
			this.addMap(winnersMap, pojo.getYear(), producers);
		} else {
			String[] producersSpl = producers.split(separator);
			for (String producer : producersSpl) {
				this.addMap(winnersMap, pojo.getYear(), producer);
			}
		}
	}
	
	private void addMap(Map<String, List<Integer>> winnersMap, Integer year, String producer) {
		if (winnersMap.containsKey(producer)) {
			winnersMap.get(producer).add(year);
		} else {
			List<Integer> indications = new ArrayList<Integer>();
			indications.add(year);
			winnersMap.put(producer, indications);
		}
	}
	
}