package br.com.worstmovies.pojo;

import java.io.Serializable;

public class WorstMoviesPojo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer year;
	
	private String title;
	
	private String studios;
	
	private String producers;
	
	private Boolean winner;

	public WorstMoviesPojo() {
		super();
	}
	
	public WorstMoviesPojo(Integer year, String title, String studios, String producers, Boolean winner) {
		super();
		
		this.year = year;
		this.title = title;
		this.studios = studios;
		this.producers = producers;
		this.winner = winner;
	}
	
	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStudios() {
		return studios;
	}

	public void setStudios(String studios) {
		this.studios = studios;
	}

	public String getProducers() {
		return producers;
	}

	public void setProducers(String producers) {
		this.producers = producers;
	}

	public Boolean isWinner() {
		return winner;
	}

	public void setWinner(Boolean winner) {
		this.winner = winner;
	}

}
